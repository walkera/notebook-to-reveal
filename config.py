import IPython.nbconvert.exporters.templateexporter as templateexporter
import mistune


def custommarkdown(source):
    return mistune.markdown(source, escape=False)


templateexporter.default_filters['custommarkdown'] = custommarkdown
