{%- extends 'basic.tpl' -%}

{%- block any_cell scoped -%}
{%- if cell.metadata.slide_type in ['slide'] -%}
    <section>
    <section>
    {{ super() }}
{%- elif cell.metadata.slide_type in ['subslide'] -%}
    <section>
    {{ super() }}
{%- elif cell.metadata.slide_type in ['-'] -%}
    {%- if cell.metadata.frag_helper in ['fragment_end'] -%}
        <div class="fragment" data-fragment-index="{{ cell.metadata.frag_number }}">
        {{ super() }}
        </div>
    {%- else -%}
        {{ super() }}
    {%- endif -%}
{%- elif cell.metadata.slide_type in ['skip'] -%}
    <div style=display:none>
    {{ super() }}
    </div>
{%- elif cell.metadata.slide_type in ['notes'] -%}
    <aside class="notes">
    {{ super() }}
    </aside>
{%- elif cell.metadata.slide_type in ['fragment'] -%}
    <div class="fragment" data-fragment-index="{{ cell.metadata.frag_number }}">
    {{ super() }}
    </div>
{%- endif -%}
{%- if cell.metadata.slide_helper in ['subslide_end'] -%}
    </section>
{%- elif cell.metadata.slide_helper in ['slide_end'] -%}
    </section>
    </section>
{%- endif -%}
{%- endblock any_cell -%}

{% block header %}
<!DOCTYPE html>
<html>
    <meta name="description" content="Productivity tips for scientists in polyglot programming environment">
    <meta name="author" content="Andrew Walker">
    <title>{{resources['metadata']['name']}} slides</title>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
    <link rel="stylesheet" href="css/reveal.css">
    <link rel="stylesheet" href="css/theme/moon.css" id="theme">

    <!-- Code syntax highlighting -->
    <link rel="stylesheet" href="lib/css/zenburn.css">
    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
  </head>
{% endblock header%}


{% block body %}
<body>
<div class="reveal">
<div class="slides">
{{ super() }}
</div>
</div>
<script src="lib/js/head.min.js"></script>
<script src="js/reveal.js"></script>
<script>
  Reveal.initialize({
    controls: true,
    progress: true,
    history: true,
    center: true,
    fragments: true,
    transition: 'convex', // none/fade/slide/convex/concave/zoom

    // Optional reveal.js plugins
    dependencies: [
      { src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },
      { src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
      { src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
      { src: 'plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
      { src: 'plugin/zoom-js/zoom.js', async: true },
      { src: 'plugin/notes/notes.js', async: true }
    ]
  });
</script>
</body>
{% endblock body %}

{% block markdowncell %}
{{ cell.source | custommarkdown | strip_files_prefix }}
{% endblock markdowncell %}

{% block in_prompt -%}
{%- endblock in_prompt %}

{% block codecell %}
<pre><code>{{ cell.source }}</code></pre>
{%- if cell.outputs -%}
<pre><code>{{ cell.outputs[0].text | ansi2html }}</code></pre>
{%- endif -%}
{%- endblock codecell %}

{% block footer %}
</html>
{% endblock footer %}
